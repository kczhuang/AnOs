/**
* @brief FLASH Sector列表
* @author chenjl
* @date 2019.07.15
*/

#include "stm32f4xx.h"

//STM32F42X/43X
#define FLASH_SECTOR_NUM	(20+3)
const long Flash_Sector_Table[FLASH_SECTOR_NUM][2] = 
{
	//1*16KΪBOOTռ
    //{0x08000000,0x08003fff},
    {0x08004000,0x08007fff},        //16K
    {0x08008000,0x0800bfff},        //16K
    {0x0800c000,0x0800ffff},        //16K
    
	{0x08010000,0x0801ffff},		//64K
	{0x08020000,0x0803ffff},		//128K
	{0x08040000,0x0805ffff},		//128K
	{0x08060000,0x0807ffff},		//128K
	{0x08080000,0x0809ffff},		//128K
	{0x080A0000,0x080Bffff},		//128K
	{0x080C0000,0x080Dffff},		//128K
	{0x080E0000,0x080Fffff},		//128K
	
	{0x08100000,0x08103fff},		//16K
	{0x08104000,0x08107fff},		//16K
	{0x08108000,0x0810Bfff},		//16K
	{0x0810C000,0x0810Ffff},		//16K
	{0x08110000,0x0811ffff},		//64K
	{0x08120000,0x0813ffff},		//128K
	{0x08140000,0x0815ffff},		//128K
	{0x08160000,0x0817ffff},		//128K
	{0x08180000,0x0819ffff},		//128K
	{0x081A0000,0x081Bffff},		//128K
	{0x081C0000,0x081Dffff},		//128K
	{0x081E0000,0x081Fffff},		//128K
};
