

/**
* @brief os event module
* @author chenjl
* @date 2019.07.02
*/

#ifndef __KEREVENT_H__
#define __KEREVENT_H__

#include "kerOs.h"
#include "Kernel.h"

#define MAXTASKNUM_PER_EVENT   (4)


typedef struct {
    char eventName[16];
    TASK_CONTEXT *waitingTasks[MAXTASKNUM_PER_EVENT];
}KER_EVENT_STR;

void kerEventModuleInit(void);
int32 kerCreateEvent(char *name);
int32 kerWaitEvent(int32 eventhandle,int32 timeout_ms);
void kerSetEvent(int32 eventhandle);

#endif
