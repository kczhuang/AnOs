
/**
* @brief 内存保护单元
* @author chenjl
* @date 2019.07.04
*/

#include "kerMpu.h"

/**
* MPU 静态配置表
*/

/**
* MPU内核初始化模块
*/
void kerMpuMdlInit(void)
{
    int32 i;
    if(MPU->TYPE == 0)
    {
        //设备不支持MPU，退出后面初始化
        return ;
    }
    __DMB();
    MPU->CTRL = 0;      //禁止掉MPU,才可以进行初始化操作
    
    /*当前全部初始化为无效，具体内容在上下文切换时设置*/
    for(i=0;i<8;i++)
    {
        MPU->RNR = i;
        MPU->RBAR = 0;
        MPU->RASR = 0;
    }
    //使能MPU,同时使能在PRIVALAGE模式下使用
    MPU->CTRL = MPU_CTRL_ENABLE_Msk | MPU_CTRL_PRIVDEFENA_Msk;
    __DSB();
    __ISB();
}

/**
* @brief 根据APP中的参数，设置MPU设置保护区域，当前固定使用4个MPU
*/
void kerSetAppMpu(TASK_CONTEXT *pApp)
{
    int32 rnr = 0;
    
    __DMB();
    MPU->CTRL = 0;
    
    for(rnr = 0;rnr<4;rnr++)
    {
        MPU->RNR  = rnr;
        MPU->RBAR = pApp->mpu_bar[rnr];
        MPU->RASR = pApp->mpu_asr[rnr];
    }
    
    //使能MPU,同时使能在PRIVALAGE模式下使用
    MPU->CTRL = MPU_CTRL_ENABLE_Msk | MPU_CTRL_PRIVDEFENA_Msk;
    __DSB();
    __ISB();
}

/**
* @brief 复位MPU的寄存器值
*/
void kerMpuResetSetting(void)
{
    int32 rnr = 0;
    __DMB();
    MPU->CTRL = 0; 
    
    for(rnr=0;rnr<8;rnr++)
    {
        MPU->RNR  = rnr;
        MPU->RASR = 0;
    }
    
    //使能MPU,同时使能在PRIVALAGE模式下使用
    MPU->CTRL = MPU_CTRL_ENABLE_Msk | MPU_CTRL_PRIVDEFENA_Msk;
    __DSB();
    __ISB();
}

/**
* @brief MPU设置保护区域
*/
void kerMpuSet(uint32 romAddr,uint32 romSize,uint32 ramAddr,uint32 ramSize)
{
    uint32 ramASR = 0x13060001;  //XN=1,AP=3,S=1,C=1,B=0
    uint32 romASR = 0x06020001;  //XN=0,AP=6,S=0,C=1,B=0
    int32  regidx;
    uint8  romRegion = 31;
    uint8  ramRegion = 31;
    if(MPU->TYPE == 0)
    {
        //设备不支持MPU，退出后面初始化
        return ;
    }
    /*参数安全检查*/
    for(regidx=4;regidx<32;regidx++)
    {
        if((1<<(regidx+1)) >= romSize)
        {
            romRegion = regidx;
            break;
        }
    }
    
    for(regidx=4;regidx<32;regidx++)
    {
        if((1<<(regidx+1)) >= ramSize)
        {
            ramRegion = regidx;
            break;
        }
    }
    
    romAddr = romAddr & ~((1<<(1+romRegion))-1);
    ramAddr = ramAddr & ~((1<<(1+ramRegion))-1);
    
    //禁止掉MPU,才可以进行初始化操作
    __DMB();
    MPU->CTRL = 0;      
    //空间映射操作
    MPU->RNR  = 0;
    MPU->RBAR = romAddr;
    MPU->RASR = romASR | (romRegion<<1);
    
    MPU->RNR  = 1;
    MPU->RBAR = ramAddr;
    MPU->RASR = ramASR | (ramRegion<<1);
    
    //使能MPU,同时使能在PRIVALAGE模式下使用
    MPU->CTRL = MPU_CTRL_ENABLE_Msk | MPU_CTRL_PRIVDEFENA_Msk;
    __DSB();
    __ISB();
}
