
/**
* @brief 操作系统头文件
* @author chenjl
* @date 2019.07.01
*/

#ifndef __KEROS_H__
#define __KEROS_H__

#include "stm32f4xx.h"

typedef unsigned long  uint32;
typedef signed   long  int32;
typedef unsigned short uint16;
typedef signed   short int16;
typedef unsigned char  uint8;
typedef signed   char  int8;

#define EVENT_HANDLE_FLAG       (0x01100000)
#define DEV_HANDLE_FLAG         (0x01200000)
#define HANDLE_MASK             (0xfff00000)
#define HANDLE_RELMASK          (0x000fffff)
/**
* @brief 向应用程序提供的接口函数定义
*/

#endif
