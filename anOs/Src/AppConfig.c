
/**
* @brief 应用程序配置文件
* @author chenjl
* @date 2019.07.15
*/

#include "AppConfig.h"

__weak int32 app_num = 2;

__weak APPCONFIG_STR app_configures[] = 
{
    {
        //.ram_start
        0x20000000,
        //.ram_size
        0x1000,
        //.stack_size
        0x400,
        //.rom_start
        0x08020000,
        //.rom_size
        0x20000,
        //.period_ms
        1000,
        //.priority
        10,
        //.share_mem_access
        0
    },
    {
        //.ram_start
        0x20001000,
        //.ram_size
        0x1000,
        //.stack_size
        0x400,
        //.rom_start
        0x08040000,
        //.rom_size
        0x20000,
        //.period_ms
        1000,
        //.priority
        10,
        //.share_mem_access
        0
    }
};
