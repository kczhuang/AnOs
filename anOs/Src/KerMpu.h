
/**
* @brief 内存保护单元
* @author chenjl
* @date 2019.07.04
*/


#ifndef __MPU_H__
#define __MPU_H__
#include "kerOs.h"
#include "Kernel.h"

extern void kerMpuMdlInit(void);
extern void kerMpuSet(uint32 romAddr,uint32 romSize,uint32 ramAddr,uint32 ramSize);
void kerSetAppMpu(TASK_CONTEXT *pApp);
void kerMpuResetSetting(void);

#endif
