
/**
* @brief 操作系统参数配置文件
* @author chenjl
* @date 2019.07.01
*/

#include "OsConfigure.h"
#include "task.h"
#include "string.h"
#include "AppConfig.h"

TASK_CONTEXT taskConfigurations[TASK_MAX_NUM];

int32   task_init_num = 0;
static uint32 MemsizeToRegion(uint32 memsize);
static void kerAppInitMpu(TASK_CONTEXT *pApp,
    uint32 romAddr,uint32 romSize,
    uint32 ramAddr,uint32 ramSize,
    uint32 shareAddr,uint32 shareSize,uint8 shareAttri);

uint32 ShareMemStartAddr = 0x20028000;
uint32 ShareMemSize      = 0x8000;

/**
* @brief 加载全局配置参数信息
* @details 全局参数配置，包括：
分区数，每个分区的代码地址，代码空间大小:
RAM起始地址，RAM大小，RAM栈大小
每个任务的周期时间；任务状态；优先级；
以及外设硬件的工作参数
*/
void LoadConfiguration(void)
{
    int32 cnt = 0;
    EXCEPTION_CONTEXT *pException;
    
    for(cnt=0;cnt < app_num && cnt <TASK_MAX_NUM;cnt++ )
    {
        taskConfigurations[cnt].ram_start = app_configures[cnt].ram_start;
        taskConfigurations[cnt].ram_size  = app_configures[cnt].ram_size;
        taskConfigurations[cnt].stack_size= app_configures[cnt].stack_size;
        taskConfigurations[cnt].rom_start = app_configures[cnt].rom_start+1;
        taskConfigurations[cnt].rom_size  = app_configures[cnt].rom_size;
        taskConfigurations[cnt].period    = MS_TO_TICK(app_configures[cnt].period_ms);
        taskConfigurations[cnt].priority  = app_configures[cnt].priority;
        taskConfigurations[cnt].shmAttri  = app_configures[cnt].share_mem_access;
    }    
    //重置最大任务数，作数据保护
    task_init_num = cnt;
    
    /*内部变量的计算和初始化*/
    for(cnt = 0;cnt < task_init_num;cnt++)
    {
        taskConfigurations[cnt].stack_top = taskConfigurations[cnt].ram_start +
            taskConfigurations[cnt].ram_size;
        taskConfigurations[cnt].stack_buttom = (uint32*)(taskConfigurations[cnt].stack_top -
            taskConfigurations[cnt].stack_size);
        taskConfigurations[cnt].stack_ptr = taskConfigurations[cnt].stack_top -
            sizeof(EXCEPTION_CONTEXT);
        taskConfigurations[cnt].stack_ptr &= 0xfffffff8;        //栈指针对齐-double word stack alignment
        *taskConfigurations[cnt].stack_buttom = STACK_END_FLAG;  //放置栈保护标志
        
        taskConfigurations[cnt].period_time = taskConfigurations[cnt].period;
        taskConfigurations[cnt].wait_time = -1;
        taskConfigurations[cnt].touchtick = 0;
        taskConfigurations[cnt].status = TASK_STATUS_READY;
        taskConfigurations[cnt].timtick = 0;
        
        /*初始化任务的启动栈信息*/
        pException = (EXCEPTION_CONTEXT*)taskConfigurations[cnt].stack_ptr;
        memset(pException,0,sizeof(EXCEPTION_CONTEXT));
        pException->pc = taskConfigurations[cnt].rom_start; 
        pException->lr = pException->pc;                        //LR=PC,在程序分区退出后，重新进入循环
        pException->xpsr = 0x21000000;
        
        /*初始化MPU寄存器值*/
        kerAppInitMpu(&taskConfigurations[cnt],
                taskConfigurations[cnt].rom_start,
                taskConfigurations[cnt].rom_size,
                taskConfigurations[cnt].ram_start,
                taskConfigurations[cnt].ram_size,
                ShareMemStartAddr,
                ShareMemSize,
                taskConfigurations[cnt].shmAttri);                
    }  
}

/**
* @brief 初始化任务的MPU寄存器
* @details 输入ROM起始地址和大小，RAM起始地址和大小，
*      共享内存起始地址，大小，和读写属性区域
* @input shareAttri，将shareSize平分成8块，每块对应的Attri为
*      0时表示可读写，1时表示只读。
*/
static void kerAppInitMpu(TASK_CONTEXT *pApp,
    uint32 romAddr,uint32 romSize,
    uint32 ramAddr,uint32 ramSize,
    uint32 shareAddr,uint32 shareSize,uint8 shareAttri
)
{
#define EXEC_ASR        (0x06020001) //XN=0,AP=6,S=0,C=1,B=0
#define RW_ASR          (0x13060001) //XN=1,AP=3,S=1,C=1,B=0
#define RO_ASR          (0x16060001) //XN=1,AP=6,S=1,C=1,B=0 
    
    uint32 romRegion = MemsizeToRegion(romSize);
    uint32 ramRegion = MemsizeToRegion(ramSize);
    uint32 shmRegion = MemsizeToRegion(shareSize);
    
    romAddr = romAddr & ~((1<<(1+romRegion))-1);
    ramAddr = ramAddr & ~((1<<(1+ramRegion))-1);
    shareAddr = shareAddr & ~((1<<(1+shmRegion))-1);
    
    pApp->mpu_bar[0] = romAddr;
    pApp->mpu_bar[1] = ramAddr;
    pApp->mpu_bar[2] = shareAddr;
    pApp->mpu_bar[3] = shareAddr;           //覆盖前面一条，通过属性屏蔽使用不同的属性
    
    if(romSize > 0)
    {
        pApp->mpu_asr[0] = EXEC_ASR | (romRegion<<1);
    }
    else
    {
        pApp->mpu_asr[0] = 0;
    }
    if(ramSize > 0)
    {
        pApp->mpu_asr[1] = RW_ASR   | (ramRegion<<1);
    }
    else
    {
        pApp->mpu_asr[1] = 0;
    }
    if(shareSize > 0)
    {
        pApp->mpu_asr[2] = RO_ASR   | (shmRegion<<1);
        pApp->mpu_asr[3] = RW_ASR   | (shmRegion<<1) | (shareAttri << 8);
    }
    else
    {
        //没有共享内存配置
        pApp->mpu_asr[2] = 0;
        pApp->mpu_asr[3] = 0;
    }
    
    pApp->mpu_asr[4] = 0;
    pApp->mpu_asr[5] = 0;
    pApp->mpu_asr[6] = 0;
    pApp->mpu_asr[7] = 0;
}

/**
* @brief 根据内存大小，转换为需要使用的参数值
*/
static uint32 MemsizeToRegion(uint32 memsize)
{
    uint32 regidx;
    uint32 region = 31;
    for(regidx=4;regidx<32;regidx++)
    {
        if((1<<(regidx+1)) >= memsize)
        {
            region = regidx;
            break;
        }
    }
    return region;
}
