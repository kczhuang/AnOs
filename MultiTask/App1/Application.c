/**
* @brief 分区应用示例1
* @author chenjl
* @date 2019.07.01
*/

#include "anOs.h"

int app_var_1 = 2;

int app_data  = 3;
char app_name[20] = "this_is_app_string";

void AppMain(void)
{
    int32 devled = dev_open("LED",0);
    
    while(1)
    {
        app_var_1 = 1;
        app_data += app_data;
        
        
        dev_write(devled,0,1);
        TaskWait(1000);
        dev_write(devled,0,0);
        TaskWait(1000);
    }
}
